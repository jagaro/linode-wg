#!/bin/bash

set -euo pipefail

PUBLIC_IFACE="${PUBLIC_IFACE:-eth0}"

SERVER_IPV4="$(
  ip -4 addr show dev "$PUBLIC_IFACE" | grep '^\s*inet' | head -1 |
    sed -r 's/^\s*inet\s+(.+)\/.+$/\1/'
)"
if [ -z "$SERVER_IPV4" ]; then
  echo >&2 "ERROR: Could not determine IPv4 on $PUBLIC_IFACE"
  exit 2
fi

SERVER_IPV6="$(
  ip -6 addr show dev "$PUBLIC_IFACE" | grep '^\s*inet' | head -1 |
    sed -r 's/^\s*inet6\s+(.+)\/.+$/\1/'
)"
if [ -z "$SERVER_IPV6" ]; then
  echo >&2 "ERROR: Could not determine IPv6 on $PUBLIC_IFACE"
  exit 2
fi

INSTALL_LOG_PATH=/var/log/linode-wg-install.log
SERVER_FQDN="${SERVER_FQDN:-linode-wg.example.com}"
SERVER_HOSTNAME="$(echo "$SERVER_FQDN" | cut -d. -f1)"
WG_SERVER_IFACE="${WG_SERVER_IFACE:-wg0}"
WG_SERVER_PORT="${WG_SERVER_PORT:-51820}"
WG_SERVER_IPV4="${WG_SERVER_IPV4:-10.42.42.254}"
WG_SERVER_IPV6="${WG_SERVER_IPV6:-fd42:42:42::fe}"
WG_CLIENT_IPV4="${WG_CLIENT_IPV4:-10.42.42.1}"
WG_CLIENT_IPV6="${WG_CLIENT_IPV6:-fd42:42:42::1}"
DNS_IPV4="${DNS_IPV4:-1.1.1.1}"
DNS_IPV6="${DNS_IPV6:-2606:4700:4700::1111}"

WG_SECRETS_PATH="/etc/wireguard/secrets"
WG_SERVER_CONFIG_PATH="/etc/wireguard/$WG_SERVER_IFACE.conf"
WG_CLIENT_CONFIG_PATH="/etc/wireguard/client.conf"

log() {
  echo "> $*" >>"$INSTALL_LOG_PATH"
  set +e
  "$@" >>"$INSTALL_LOG_PATH" 2>&1
  local result=$?
  set -e
  if [ $result != 0 ]; then
    echo ERROR
    echo "> $* returned $result" | tee "$INSTALL_LOG_PATH" >&2
    echo "> See $INSTALL_LOG_PATH" >&2
    exit $result
  fi
}

echo -n "Securing system..."

perl -pi -e 's/^(root):[^:]*:(.*)$/$1:!:$2/' /etc/shadow

perl -pi -e 's/PasswordAuthentication yes/PasswordAuthentication no/g' \
  /etc/ssh/sshd_config
log systemctl restart ssh

echo ok
echo -n "Upgrading system..."

log apt-get update -q
DEBIAN_FRONTEND=noninteractive log apt-get dist-upgrade -y

echo ok
echo -n "Installing packages..."
DEBIAN_FRONTEND=noninteractive log apt-get install -y \
  apt-listchanges dnsmasq nftables nmap tcpdump qrencode \
  unattended-upgrades wireguard

echo ok
echo -n "Configuring network..."

echo "$SERVER_HOSTNAME" >/etc/hostname

hostname -F /etc/hostname

cat >/etc/hosts <<EOF
127.0.0.1       localhost
127.0.1.1       $SERVER_FQDN $SERVER_HOSTNAME

# The following lines are desirable for IPv6 capable hosts
::1             localhost ip6-localhost ip6-loopback
ff02::1         ip6-allnodes
ff02::2         ip6-allrouters
EOF

cat >/etc/resolv.conf <<EOF
nameserver $DNS_IPV6
nameserver $DNS_IPV4
EOF

cat >/etc/nftables.conf <<EOF
#!/usr/sbin/nft -f

flush ruleset

table inet filter {
  chain input {
    type filter hook input priority filter; policy accept;

    iifname "lo" accept
    ct state { established, related } counter accept
    ct state invalid counter drop
    ip protocol icmp counter accept
    ip6 nexthdr ipv6-icmp counter accept

    iifname { "$PUBLIC_IFACE", "$WG_SERVER_IFACE" } tcp dport ssh counter accept
    iifname "$WG_SERVER_IFACE" tcp dport { http, https } counter accept
    iifname "$WG_SERVER_IFACE" tcp dport domain counter accept
    iifname "$WG_SERVER_IFACE" udp dport domain counter accept
    iifname "$PUBLIC_IFACE" udp dport $WG_SERVER_PORT counter accept

    counter reject
  }

  chain forward {
    type filter hook forward priority filter; policy accept;
    ct state { established, related } counter accept
    iifname "$WG_SERVER_IFACE" oifname { "$PUBLIC_IFACE", "$WG_SERVER_IFACE" } counter accept
    counter reject
  }

  chain output {
    type filter hook output priority filter; policy accept;
    counter accept
  }

  chain prerouting {
    type nat hook prerouting priority dstnat; policy accept;
    counter accept
  }

  chain postrouting {
    type nat hook postrouting priority srcnat; policy accept;
    iifname "$WG_SERVER_IFACE" oifname "$PUBLIC_IFACE" counter masquerade
    counter accept
  }
}
EOF

log systemctl enable nftables
log systemctl restart nftables

cat >/etc/sysctl.d/ip_forwarding.conf <<'EOF'
net.ipv4.ip_forward = 1
net.ipv6.conf.all.forwarding = 1
EOF
log sysctl --system

echo ok
echo -n "Configuring WireGuard..."

log systemctl enable "wg-quick@$WG_SERVER_IFACE"
log systemctl stop "wg-quick@$WG_SERVER_IFACE"

touch \
  "$WG_SECRETS_PATH" \
  "$WG_SERVER_CONFIG_PATH" \
  "$WG_CLIENT_CONFIG_PATH"
chmod 600 \
  "$WG_SECRETS_PATH" \
  "$WG_SERVER_CONFIG_PATH" \
  "$WG_CLIENT_CONFIG_PATH"

. "$WG_SECRETS_PATH"

if [ -z "${WG_SERVER_PRIVATE_KEY:-}" ]; then
  WG_SERVER_PRIVATE_KEY="$(wg genkey)"
fi

if [ -z "${WG_CLIENT_PRIVATE_KEY:-}" ]; then
  WG_CLIENT_PRIVATE_KEY="$(wg genkey)"
fi

cat >"$WG_SECRETS_PATH" <<EOF
WG_SERVER_PRIVATE_KEY=${WG_SERVER_PRIVATE_KEY@Q}
WG_CLIENT_PRIVATE_KEY=${WG_CLIENT_PRIVATE_KEY@Q}
EOF

cat >"$WG_SERVER_CONFIG_PATH" <<EOF
[Interface]
PrivateKey = $WG_SERVER_PRIVATE_KEY
Address = $WG_SERVER_IPV4/24, $WG_SERVER_IPV6/64
ListenPort = $WG_SERVER_PORT
SaveConfig = true
PostUp = systemctl restart dnsmasq

[Peer]
PublicKey = $(echo "$WG_CLIENT_PRIVATE_KEY" | wg pubkey)
AllowedIPs = $WG_CLIENT_IPV4/32, $WG_CLIENT_IPV6/128
EOF

WG_CLIENT_CONFIG="[Interface]
PrivateKey = $WG_CLIENT_PRIVATE_KEY
Address = $WG_CLIENT_IPV4/32, $WG_CLIENT_IPV6/128
DNS = $WG_SERVER_IPV4, $WG_SERVER_IPV6

[Peer]
PublicKey = $(echo "$WG_SERVER_PRIVATE_KEY" | wg pubkey)
AllowedIPs = 0.0.0.0/0, ::/0
Endpoint = $SERVER_IPV4:$WG_SERVER_PORT"

cat >"$WG_CLIENT_CONFIG_PATH" <<EOF
# WireGuard Client
#
$(
  echo "$WG_CLIENT_CONFIG" | qrencode -t UTF8i | sed 's/^/#/g' |
    tail -n +3 | head -n -2
)

$WG_CLIENT_CONFIG
EOF

log systemctl start "wg-quick@$WG_SERVER_IFACE"

echo ok
