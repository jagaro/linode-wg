# linode-wg

Simple Linode WireGuard Server.

## Setup

### 1. Clone Repository

```sh
git clone https://gitlab.com/jagaro/linode-wg.git
cd linode-wg
```

### 2. Create API Token

Create a [Linode API Token](https://cloud.linode.com/profile/tokens) with at
least the following scopes:

- Account (Read Only)
- Linodes (Read/Write)

### 3. Configure

```sh
touch .config
chmod 600 .config
echo LINODE_API_TOKEN=abcd... >.config
echo LINODE_REGION=us-west >>.config
```

## Usage

```sh
./linode-wg create
```
